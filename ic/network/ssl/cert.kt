package ic.network.ssl


import ic.annotations.RootOnly
import ic.interfaces.iterable.ext.copyConvert
import ic.interfaces.iterable.ext.join
import ic.storage.fs.Directory
import ic.storage.ic.commonPublicDataStorage
import ic.stream.sequence.ByteSequence
import ic.struct.collection.Collection
import ic.system.LocalBashSession
import ic.system.executeBashCommand
import ic.system.isRoot
import ic.throwables.NotExists
import ic.throwables.NotSupported
import ic.throwables.NotSupported.NOT_SUPPORTED


@Throws(NotSupported::class)
@RootOnly
fun generateCertJks(

	email : String,
	domainNames : Collection<String>,

	keyStorePassword : String,
	keyPassword : String

) : ByteSequence {

	if (!isRoot) throw NotSupported("Only root user can generate SSL certificates")

	commonPublicDataStorage.createFolderIfNotExists("ssl").createIfNull("certbot-installed") {
		executeBashCommand("apt install -y certbot")
		true
	}

	println(
		executeBashCommand(
			"certbot certonly " + (
				"--standalone " +
				"--non-interactive " +
				"--expand " +
				"--agree-tos " +
				"--email $email " +
				domainNames.copyConvert { "-d $it" }.join(separator = ' ')
			)
		)
	)

	val letsEncryptLiveDir = try {
		Directory.getExistingOrThrowNotExists("/etc/letsencrypt/live")
	} catch (notExists: NotExists) { throw NOT_SUPPORTED }

	val letsEncryptKeysDir : Directory = letsEncryptLiveDir[domainNames.getAny()]
	val tmpStorage = Directory.createIfNotExists("/tmp/ic/ssl")
	tmpStorage.removeIfExists("certificate.jks")
	val bashSession = LocalBashSession(letsEncryptKeysDir)
	println(
		bashSession.executeCommand("openssl pkcs12 -export -out keystore.pkcs12 -in fullchain.pem -inkey privkey.pem -password pass:ilovejava")
	)
	println(
		bashSession.executeCommand(
			"keytool -importkeystore " + (
				"-srckeystore keystore.pkcs12 " +
				"-srcstoretype PKCS12 " +
				"-destkeystore /tmp/ic/ssl/certificate.jks " +
				"-storepass $keyStorePassword " +
				"-keypass $keyPassword " +
				"-srcstorepass ilovejava"
			)
		)
	)
	println(
		bashSession.executeCommand("rm keystore.pkcs12")
	)
	return tmpStorage["certificate.jks"]

}